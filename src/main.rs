#![feature(proc_macro, conservative_impl_trait, generators)]

extern crate futures_await as futures;
extern crate hyper;
extern crate hyper_tls;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate tokio_core;
extern crate url;

mod types;

use std::env;
use hyper::Client;
use hyper::client::HttpConnector;
use hyper_tls::HttpsConnector;
use tokio_core::reactor::Core;
use url::Url;

use futures::prelude::*;

static BASE_URL: &'static str = "https://api.telegram.org/bot";

type HttpClient = Client<HttpsConnector<HttpConnector>>;

#[derive(Serialize, Deserialize, Debug)]
struct Response<T> {
    ok: bool,
    description: Option<String>,
    result: Option<T>,
}

#[async]
fn api_get<T>(
    client: HttpClient,
    path: &'static str,
    query: Vec<(&'static str, String)>,
) -> Result<T, String>
where
    T: serde::de::DeserializeOwned,
{
    let access_token = env::var("ACCESS_TOKEN").unwrap();
    let url = Url::parse_with_params(&format!("{}{}/{}", BASE_URL, access_token, path), query)
        .unwrap();

    println!("url: {:?}", url);

    let uri = url.as_str().parse().unwrap();

    // Do the HTTP request
    let http_response = await!(client.get(uri)).map_err(|e| e.to_string())?;
    let http_body = await!(http_response.body().concat2()).map_err(|e| {
        format!("http_body: {}", e.to_string())
    })?;

    // Deserialize the JSON
    let response: Response<T> = serde_json::from_slice(&http_body).map_err(|e| {
        format!("serde: {}", e.to_string())
    })?;

    response.result.ok_or("no result in response".to_string())
}

#[async]
fn get_me(client: HttpClient) -> Result<types::user::User, String> {
    await!(api_get::<types::user::User>(client, "getMe", Vec::new()))
}

#[async]
fn get_updates(client: HttpClient) -> Result<Vec<types::update::Update>, String> {
    await!(api_get::<Vec<types::update::Update>>(
        client,
        "getUpdates",
        Vec::new(),
    ))
}

#[async]
fn send_message(
    client: HttpClient,
    chat_id: String,
    text: String,
) -> Result<types::message::Message, String> {
    await!(api_get::<types::message::Message>(
        client,
        "sendMessage",
        vec![("chat_id", chat_id), ("text", text)],
    ))
}

fn main() {
    let mut core = Core::new().unwrap();
    let handle = core.handle();
    let client = Client::configure()
        .connector(HttpsConnector::new(4, &handle).unwrap())
        .build(&handle);

    let me = core.run(get_me(client.clone())).unwrap();
    println!("me: {:?}", me);

    let updates = core.run(get_updates(client.clone())).unwrap();
    for update in updates {
        println!("update: {:#?}", update);
    }
}
