pub mod animation;

pub mod audio;

pub mod callback_game;

pub mod callback_query;

pub mod chat;

pub mod chat_member;

pub mod chat_photo;

pub mod chosen_inline_result;

pub mod contact;

pub mod document;

pub mod file;

pub mod force_reply;

pub mod game;

pub mod game_high_score;

pub mod inline_keyboard_button;

pub mod inline_keyboard_markup;

pub mod inline_query;

pub mod inline_query_result;

pub mod inline_query_result_article;

pub mod inline_query_result_audio;

pub mod inline_query_result_cached_audio;

pub mod inline_query_result_cached_document;

pub mod inline_query_result_cached_gif;

pub mod inline_query_result_cached_mpeg4_gif;

pub mod inline_query_result_cached_photo;

pub mod inline_query_result_cached_sticker;

pub mod inline_query_result_cached_video;

pub mod inline_query_result_cached_voice;

pub mod inline_query_result_contact;

pub mod inline_query_result_document;

pub mod inline_query_result_game;

pub mod inline_query_result_gif;

pub mod inline_query_result_location;

pub mod inline_query_result_mpeg4_gif;

pub mod inline_query_result_photo;

pub mod inline_query_result_venue;

pub mod inline_query_result_video;

pub mod inline_query_result_voice;

pub mod input_contact_message_content;

pub mod input_location_message_content;

pub mod input_media;

pub mod input_media_photo;

pub mod input_media_video;

pub mod input_message_content;

pub mod input_text_message_content;

pub mod input_venue_message_content;

pub mod invoice;

pub mod keyboard_button;

pub mod labeled_price;

pub mod location;

pub mod mask_position;

pub mod message;

pub mod message_entity;

pub mod order_info;

pub mod photo_size;

pub mod pre_checkout_query;

pub mod reply_keyboard_markup;

pub mod reply_keyboard_remove;

pub mod response_parameters;

pub mod shipping_address;

pub mod shipping_option;

pub mod shipping_query;

pub mod sticker;

pub mod sticker_set;

pub mod successful_payment;

pub mod update;

pub mod user;

pub mod user_profile_photos;

pub mod venue;

pub mod video;

pub mod video_note;

pub mod voice;

