use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a link to a sticker stored on the Telegram servers. By default,
/// this sticker will be sent by the user. Alternatively, you can use
/// input_message_content to send a message with the specified content instead
/// of the sticker.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultCachedSticker {
    /// Type of the result, must be sticker
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 bytes
    pub id: String,

    /// A valid file identifier of the sticker
    pub sticker_file_id: String,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the sticker
    pub input_message_content: Option<InputMessageContent>
}

