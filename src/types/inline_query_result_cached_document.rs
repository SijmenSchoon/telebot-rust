use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a link to a file stored on the Telegram servers. By default,
/// this file will be sent by the user with an optional caption. Alternatively,
/// you can use input_message_content to send a message with the specified
/// content instead of the file.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultCachedDocument {
    /// Type of the result, must be document
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 bytes
    pub id: String,

    /// Title for the result
    pub title: String,

    /// A valid file identifier for the file
    pub document_file_id: String,

    /// Short description of the result
    pub description: Option<String>,

    /// Caption of the document to be sent, 0-200 characters
    pub caption: Option<String>,

    /// Send Markdown or HTML, if you want Telegram apps to show bold, italic,
    /// fixed-width text or inline URLs in the media caption.
    pub parse_mode: Option<String>,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the file
    pub input_message_content: Option<InputMessageContent>
}

