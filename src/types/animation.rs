use types::photo_size::PhotoSize;

/// You can provide an animation for your game so that it looks stylish in
/// chats (check out Lumberjack for an example). This object represents an
/// animation file to be displayed in the message containing a game.
#[derive(Serialize, Deserialize, Debug)]
pub struct Animation {
    /// Unique file identifier
    pub file_id: String,

    /// Animation thumbnail as defined by sender
    pub thumb: Option<PhotoSize>,

    /// Original animation filename as defined by sender
    pub file_name: Option<String>,

    /// MIME type of the file as defined by sender
    pub mime_type: Option<String>,

    /// File size
    pub file_size: Option<i64>
}

