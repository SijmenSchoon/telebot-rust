use types::shipping_address::ShippingAddress;

/// This object represents information about an order.
#[derive(Serialize, Deserialize, Debug)]
pub struct OrderInfo {
    /// User name
    pub name: Option<String>,

    /// User's phone number
    pub phone_number: Option<String>,

    /// User email
    pub email: Option<String>,

    /// User shipping address
    pub shipping_address: Option<ShippingAddress>
}

