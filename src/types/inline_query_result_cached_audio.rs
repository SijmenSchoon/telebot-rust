use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a link to an mp3 audio file stored on the Telegram servers. By
/// default, this audio file will be sent by the user. Alternatively, you can
/// use input_message_content to send a message with the specified content
/// instead of the audio.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultCachedAudio {
    /// Type of the result, must be audio
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 bytes
    pub id: String,

    /// A valid file identifier for the audio file
    pub audio_file_id: String,

    /// Caption, 0-200 characters
    pub caption: Option<String>,

    /// Send Markdown or HTML, if you want Telegram apps to show bold, italic,
    /// fixed-width text or inline URLs in the media caption.
    pub parse_mode: Option<String>,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the audio
    pub input_message_content: Option<InputMessageContent>
}

