use types::location::Location;

/// This object represents a venue.
#[derive(Serialize, Deserialize, Debug)]
pub struct Venue {
    /// Venue location
    pub location: Location,

    /// Name of the venue
    pub title: String,

    /// Address of the venue
    pub address: String,

    /// Foursquare identifier of the venue
    pub foursquare_id: Option<String>
}

