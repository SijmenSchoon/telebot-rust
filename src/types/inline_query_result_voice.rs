use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a link to a voice recording in an .ogg container encoded with
/// OPUS. By default, this voice recording will be sent by the user.
/// Alternatively, you can use input_message_content to send a message with the
/// specified content instead of the the voice message.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultVoice {
    /// Type of the result, must be voice
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 bytes
    pub id: String,

    /// A valid URL for the voice recording
    pub voice_url: String,

    /// Recording title
    pub title: String,

    /// Caption, 0-200 characters
    pub caption: Option<String>,

    /// Send Markdown or HTML, if you want Telegram apps to show bold, italic,
    /// fixed-width text or inline URLs in the media caption.
    pub parse_mode: Option<String>,

    /// Recording duration in seconds
    pub voice_duration: Option<i64>,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the voice recording
    pub input_message_content: Option<InputMessageContent>
}

