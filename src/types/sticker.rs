use types::mask_position::MaskPosition;
use types::photo_size::PhotoSize;

/// This object represents a sticker.
#[derive(Serialize, Deserialize, Debug)]
pub struct Sticker {
    /// Unique identifier for this file
    pub file_id: String,

    /// Sticker width
    pub width: i64,

    /// Sticker height
    pub height: i64,

    /// Sticker thumbnail in the .webp or .jpg format
    pub thumb: Option<PhotoSize>,

    /// Emoji associated with the sticker
    pub emoji: Option<String>,

    /// Name of the sticker set to which the sticker belongs
    pub set_name: Option<String>,

    /// For mask stickers, the position where the mask should be placed
    pub mask_position: Option<MaskPosition>,

    /// File size
    pub file_size: Option<i64>
}

