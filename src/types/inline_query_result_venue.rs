use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a venue. By default, the venue will be sent by the user.
/// Alternatively, you can use input_message_content to send a message with the
/// specified content instead of the venue.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultVenue {
    /// Type of the result, must be venue
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 Bytes
    pub id: String,

    /// Latitude of the venue location in degrees
    pub latitude: f64,

    /// Longitude of the venue location in degrees
    pub longitude: f64,

    /// Title of the venue
    pub title: String,

    /// Address of the venue
    pub address: String,

    /// Foursquare identifier of the venue if known
    pub foursquare_id: Option<String>,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the venue
    pub input_message_content: Option<InputMessageContent>,

    /// Url of the thumbnail for the result
    pub thumb_url: Option<String>,

    /// Thumbnail width
    pub thumb_width: Option<i64>,

    /// Thumbnail height
    pub thumb_height: Option<i64>
}

