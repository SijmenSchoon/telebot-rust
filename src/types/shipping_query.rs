use types::shipping_address::ShippingAddress;
use types::user::User;

/// This object contains information about an incoming shipping query.
#[derive(Serialize, Deserialize, Debug)]
pub struct ShippingQuery {
    /// Unique query identifier
    pub id: String,

    /// User who sent the query
    pub from: User,

    /// Bot specified invoice payload
    pub invoice_payload: String,

    /// User specified shipping address
    pub shipping_address: ShippingAddress
}

