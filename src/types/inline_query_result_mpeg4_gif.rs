use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a link to a video animation (H.264/MPEG-4 AVC video without
/// sound). By default, this animated MPEG-4 file will be sent by the user with
/// optional caption. Alternatively, you can use input_message_content to send
/// a message with the specified content instead of the animation.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultMpeg4Gif {
    /// Type of the result, must be mpeg4_gif
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 bytes
    pub id: String,

    /// A valid URL for the MP4 file. File size must not exceed 1MB
    pub mpeg4_url: String,

    /// Video width
    pub mpeg4_width: Option<i64>,

    /// Video height
    pub mpeg4_height: Option<i64>,

    /// Video duration
    pub mpeg4_duration: Option<i64>,

    /// URL of the static thumbnail (jpeg or gif) for the result
    pub thumb_url: String,

    /// Title for the result
    pub title: Option<String>,

    /// Caption of the MPEG-4 file to be sent, 0-200 characters
    pub caption: Option<String>,

    /// Send Markdown or HTML, if you want Telegram apps to show bold, italic,
    /// fixed-width text or inline URLs in the media caption.
    pub parse_mode: Option<String>,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the video animation
    pub input_message_content: Option<InputMessageContent>
}

