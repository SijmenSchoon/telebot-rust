use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a link to an animated GIF file stored on the Telegram servers.
/// By default, this animated GIF file will be sent by the user with an
/// optional caption. Alternatively, you can use input_message_content to send
/// a message with specified content instead of the animation.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultCachedGif {
    /// Type of the result, must be gif
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 bytes
    pub id: String,

    /// A valid file identifier for the GIF file
    pub gif_file_id: String,

    /// Title for the result
    pub title: Option<String>,

    /// Caption of the GIF file to be sent, 0-200 characters
    pub caption: Option<String>,

    /// Send Markdown or HTML, if you want Telegram apps to show bold, italic,
    /// fixed-width text or inline URLs in the media caption.
    pub parse_mode: Option<String>,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the GIF animation
    pub input_message_content: Option<InputMessageContent>
}

