/// Represents a video to be sent.
#[derive(Serialize, Deserialize, Debug)]
pub struct InputMediaVideo {
    /// Type of the result, must be video
    #[serde(rename="type")]
    pub type_: String,

    /// File to send. Pass a file_id to send a file that exists on the Telegram
    /// servers (recommended), pass an HTTP URL for Telegram to get a file from
    /// the Internet, or pass "attach://<file_attach_name>" to upload a new one
    /// using multipart/form-data under <file_attach_name> name.
    pub media: String,

    /// Caption of the video to be sent, 0-200 characters
    pub caption: Option<String>,

    /// Send Markdown or HTML, if you want Telegram apps to show bold, italic,
    /// fixed-width text or inline URLs in the media caption.
    pub parse_mode: Option<String>,

    /// Video width
    pub width: Option<i64>,

    /// Video height
    pub height: Option<i64>,

    /// Video duration
    pub duration: Option<i64>,

    /// Pass True, if the uploaded video is suitable for streaming
    pub supports_streaming: Option<bool>
}

