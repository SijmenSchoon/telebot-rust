/// This object represents the content of a message to be sent as a result of
/// an inline query. Telegram clients currently support the following 4 types:
#[derive(Serialize, Deserialize, Debug)]
pub struct InputMessageContent {
    /// Text of the message to be sent, 1-4096 characters
    pub message_text: String,

    /// Send Markdown or HTML, if you want Telegram apps to show bold, italic,
    /// fixed-width text or inline URLs in your bot's message.
    pub parse_mode: Option<String>,

    /// Disables link previews for links in the sent message
    pub disable_web_page_preview: Option<bool>
}

