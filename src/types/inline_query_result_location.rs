use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a location on a map. By default, the location will be sent by
/// the user. Alternatively, you can use input_message_content to send a
/// message with the specified content instead of the location.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultLocation {
    /// Type of the result, must be location
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 Bytes
    pub id: String,

    /// Location latitude in degrees
    pub latitude: f64,

    /// Location longitude in degrees
    pub longitude: f64,

    /// Location title
    pub title: String,

    /// Period in seconds for which the location can be updated, should be
    /// between 60 and 86400.
    pub live_period: Option<i64>,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the location
    pub input_message_content: Option<InputMessageContent>,

    /// Url of the thumbnail for the result
    pub thumb_url: Option<String>,

    /// Thumbnail width
    pub thumb_width: Option<i64>,

    /// Thumbnail height
    pub thumb_height: Option<i64>
}

