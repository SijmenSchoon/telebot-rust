use types::inline_keyboard_markup::InlineKeyboardMarkup;
use types::input_message_content::InputMessageContent;

/// Represents a link to a video file stored on the Telegram servers. By
/// default, this video file will be sent by the user with an optional caption.
/// Alternatively, you can use input_message_content to send a message with the
/// specified content instead of the video.
#[derive(Serialize, Deserialize, Debug)]
pub struct InlineQueryResultCachedVideo {
    /// Type of the result, must be video
    #[serde(rename="type")]
    pub type_: String,

    /// Unique identifier for this result, 1-64 bytes
    pub id: String,

    /// A valid file identifier for the video file
    pub video_file_id: String,

    /// Title for the result
    pub title: String,

    /// Short description of the result
    pub description: Option<String>,

    /// Caption of the video to be sent, 0-200 characters
    pub caption: Option<String>,

    /// Send Markdown or HTML, if you want Telegram apps to show bold, italic,
    /// fixed-width text or inline URLs in the media caption.
    pub parse_mode: Option<String>,

    /// Inline keyboard attached to the message
    pub reply_markup: Option<InlineKeyboardMarkup>,

    /// Content of the message to be sent instead of the video
    pub input_message_content: Option<InputMessageContent>
}

