import os
import re
import shutil
import textwrap
from typing import Set, Optional, List, Tuple, Dict

import requests
from bs4 import BeautifulSoup

URL = 'https://core.telegram.org/bots/api'

TYPE_MAP = {
    'String': 'String',
    'Integer': 'i64',
    'Boolean': 'bool',
    'True': 'bool',
    'Float': 'f64',
    'Float number': 'f64'
}


class Module:
    def __init__(self) -> None:
        self.types: Dict[str, Optional[Struct]] = {}
        self.files: Set[File] = set()

    def determine_imports(self, file_) -> None:
        for struct in file_.structs:
            type_stack: List[Type] = [v.type for v in struct.variables]
            while type_stack:
                type_ = type_stack.pop()
                for parameter in type_.parameters:
                    type_stack.append(parameter)

                if type_.type in self.types and struct.name != type_.type:
                    name = f'types::{to_snake(type_.type)}::{type_.type}'
                    file_.uses.add(name)

    def find_structs(self) -> None:
        for type_ in self.types:
            for file_ in self.files:
                for struct in file_.structs:
                    if struct.name == type_:
                        self.types[type_] = struct
                        break


class File:
    def __init__(self) -> None:
        self.mods: Set[Tuple[bool, str]] = set()
        self.uses: Set[str] = set()
        self.structs: List[Struct] = []

    def __str__(self) -> str:
        string = ''
        for public, import_ in sorted(self.mods):
            if public:
                string += 'pub '

            string += f'mod {import_};\n\n'

        for use in sorted(self.uses):
            string += f'use {use};\n'
        if self.uses:
            string += '\n'

        for struct in self.structs:
            string += f'{struct}\n\n'

        return string


class Type:
    def __init__(self, type_: Optional[str] = None,
                 parameters: Optional[List] = None) -> None:
        self.type = type_
        self.parameters: List[Type] = parameters if parameters else []
        self.reference: bool = False

    def parse_telegram_type(self, type_: str, optional: bool = False) -> None:
        if optional:
            self.type = 'Option'

            subtype = Type()
            subtype.parse_telegram_type(type_, False)
            self.parameters = [subtype]
            return

        if type_.startswith('Array of '):
            self.type = 'Vec'

            subtype = Type()
            subtype.parse_telegram_type(type_[9:])
            self.parameters = [subtype]
            return

        self.type = TYPE_MAP.get(type_) or type_

    def types(self) -> Set[str]:
        type_set: Set[str] = set()
        type_set.add(self.type)
        for parameter in self.parameters:
            type_set.union(parameter.types())
        return type_set

    def __str__(self) -> str:
        string = self.type

        if self.parameters:
            arguments = ', '.join(str(arg) for arg in self.parameters)
            string += f'<{arguments}>'

        return string


class Struct:
    def __init__(self) -> None:
        self.name: str = ''
        self.variables: List[Variable] = []
        self.documentation: str = ''

    def __str__(self) -> str:
        string = ''

        for line in textwrap.wrap(self.documentation, 75):
            string += f'/// {line}\n'

        string += f'#[derive(Serialize, Deserialize, Debug)]\n'
        string += f'pub struct {self.name} {{\n'
        string += ',\n\n'.join(str(var) for var in self.variables)
        string += '\n}'

        return string


class Variable:
    def __init__(self, name: str = '', type_: Optional[Type] = None,
                 documentation: str = '') -> None:
        self.name: str = name
        self.type: Optional[Type] = type_
        self.documentation: str = documentation
        self.rename: Optional[str] = ''

    def __str__(self) -> str:
        string = ''

        for line in textwrap.wrap(self.documentation, 71):
            string += f'    /// {line}\n'
        if self.rename:
            string += f'    #[serde(rename="{self.rename}")]\n'
        string += f'    {self.name}: {self.type}'

        return string


KEYWORDS = [
    'type'
]

MORE_ABOUT_RE = re.compile('(More info on|More about) [a-zA-Z ]* »')

EXCLUDED = set([
    'CallbackGame',
    'InputFile',

    'Sending files',
    'Inline mode objects',
    'Formatting options',
    'Inline mode methods'
])


def to_snake(name):
    snek = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', snek).lower()


def main() -> None:
    shutil.rmtree('types', ignore_errors=True)
    os.mkdir('types')

    text = requests.get(URL).text
    soup = BeautifulSoup(text, 'html.parser')

    module = Module()
    mod_file = File()

    relevant_headings = []

    # Generate the type map (for importing types)
    types_heading = soup.find(text='Available types').parent
    type_headings = types_heading.find_all_next('h4')
    for type_heading in type_headings:
        type_name = type_heading.find(text=True)
        if type_name[0].islower():
            continue
        if type_name in EXCLUDED:
            continue

        module.types[type_name] = None
        relevant_headings.append(type_heading)
        print(type_name)
    print('----------------')

    # Generate the structs
    for type_heading in relevant_headings:
        # Determine the name of the struct
        type_name = str(type_heading.find(text=True))
        snake_name = to_snake(type_name)
        print('generating', type_name)

        struct = Struct()
        struct.documentation = type_heading.find_next('p').text
        struct.name = type_name

        file_ = File()
        file_.structs.append(struct)

        module.files.add(file_)

        table = type_heading.find_next('table').find('tbody')
        for row in table.find_all('tr')[1:]:
            cols = row.find_all('td')
            assert len(cols) == 3

            variable = Variable()

            # Determine the name, and scape" keywords.
            variable.name = cols[0].text
            if variable.name in KEYWORDS:
                variable.rename = variable.name
                variable.name += '_'

            # Clean up the variable description
            documentation: str = cols[2].text.replace('Optional. ', '')
            variable.documentation = \
                MORE_ABOUT_RE.sub('', documentation).strip()

            # Mark the type as optional when the description says so
            optional: bool = cols[2].text.startswith('Optional. ')
            variable.type = Type()
            variable.type.parse_telegram_type(cols[1].text, optional)

            struct.variables.append(variable)

        module.determine_imports(file_)

        print(type_name)

    for file_ in module.files:
        for struct in file_.structs:
            module.types[struct.name] = struct

    for file_ in module.files:
        snake_name = to_snake(file_.structs[0].name)

        # Write the struct to file.
        with open(f'types/{snake_name}.rs', 'w') as file_out:
            file_out.write(str(file_))

        # Add the struct to the mod.rs file.
        mod_file.mods.add((True, snake_name))

    with open(f'types/mod.rs', 'w') as file_out:
        file_out.write(str(mod_file))


main()
